- Took the copy of antora-ui-default and renamed to felix-ui-default
- Merged in the changes made for the antora website as specified in supplemental-ui of docs.antora.org
- Removed the antora search engine and added lunr
- added free part of font-awesome 5.15.2
- Fixed up all items that are FELIX related

Menubar in source/partials/header-content.hbs

after installing npm etc etc, run:

```
gulp
```

upload result by committing

Remove cache:

(MacOS)
rm -fr ~/Library/Caches/antora


OR

```
gulp preview
```

look with webbrowser
